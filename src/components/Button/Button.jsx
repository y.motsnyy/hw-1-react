import React, { Component } from 'react'

class Button extends Component {
  render() {
    const { backgroundColor, text, onClick, id, className, ...restProps} = this.props
    
    return ( 
     <button {...restProps} style={{backgroundColor}} className={className} id={id} onClick={onClick}>{text}</button>
   );
  }
}

export default Button;
