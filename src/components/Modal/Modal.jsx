import React, { Component } from 'react'
import style from './Modal.module.scss'

export default class Modal extends Component {

  render() {
    const{header, text, actions, onClick, backgroundColor} = this.props
   
    return (
      <> 
        <div onClick={onClick} className={style.modal__wrapper}></div>
        <div  className={style.modal} style={{backgroundColor}}>
            <div className={style.modal__header}>
              <h1 className={style.modal__title}>{header}</h1>
              <button onClick={onClick} className={style.modal__close_btn}>X</button>
            </div>
            <p className={style.modal__content}>{text}</p>
            <div className={style.modal__btns}>{actions}</div>
        </div>
      </>
    )
  }
}
