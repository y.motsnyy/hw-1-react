import {FIRST_MODAL_ID, SECOND_MODAL_ID} from '../constants'

export const config = {

  [FIRST_MODAL_ID]: {
    header: 'Do you want to delete this file?',
    text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?'
    
  },

  [SECOND_MODAL_ID]: {
    header: 'Do you want to upload a new file?',
    text: 'The file may contain unwanted content! Are you sure you want to upload it?',
    backgroundColor: '#d4c154'
  }
}

