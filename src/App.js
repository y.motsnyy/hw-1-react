import './App.scss'; 
import React, {Component} from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import {config as modalConfig} from './components/modalConfig'
import {FIRST_MODAL_ID, SECOND_MODAL_ID} from './constants'

class App extends Component {

    state={
      openModalId: ''
    }
  
  closeBtn = () => {
    return this.setState(
      (state) =>
        (state = {
          ...state,
          openModalId: ''
        })
    );
  }

  actions = [
    <Button   
      text={'Ok'}  
      onClick={()=> {alert(' DONE!')}} 
      backgroundColor={'#b13118'}
      className={'modal-btn'}
      key={'okBtn'}
    />, 

    <Button 
      text={'Cancel'}
      onClick={this.closeBtn}
      backgroundColor={'#b13118'}
      className={'modal-btn'}
      key={'closeBtn'}
    />
  ]
 
  handleClick = (e) => {
        return this.setState(
      (state) =>
        (state = {
          ...state,
          openModalId: e.target.dataset.modalid
        })
    );
  };

  render () {
    const {openModalId} = this.state;
    return (
      <div className='wrapper'>
        <Button 
          text={'Open first modal'}
          onClick={this.handleClick}
          backgroundColor={'green'}
          className={'main-btn'}
          id={'openfirstmodal'}
          data-modalid={FIRST_MODAL_ID}
        />
        <Button 
          text={'Open second modal'}
          onClick={this.handleClick}
          backgroundColor={'blue'}
          className={'main-btn'}
          id={'opensecondmodal'}
          data-modalid={SECOND_MODAL_ID}
          />

        {openModalId && <Modal {
          ...modalConfig[openModalId]
        }
          actions={this.actions}
          onClick={this.closeBtn}
        /> }

      </div>
    )
  }
}

export default App